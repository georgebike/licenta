#include "SensorsModule_hh.h"

uint16_t computeWheelSpeed(volatile uint16_t wheelTimerTicksBuf[TIMER_TICKS_BUFFER_SIZE]) {
	uint32_t timerTicksTotal = 0;
	uint16_t timerTicksAvg = 0;
	uint32_t wheelCircumference_microMeters;
	uint32_t timeForRotation_microSecs;
	uint16_t actualWheelSpeed = 0;

	/* Calculate average over the wheelTimerTicksBuf buffer */
	for (uint8_t i = 0; i < TIMER_TICKS_BUFFER_SIZE; i++) {
		timerTicksTotal += wheelTimerTicksBuf[i];
	}
	timerTicksAvg = timerTicksTotal / TIMER_TICKS_BUFFER_SIZE;

	/* Calculate the actual speed in m/s (!!! scaled values to avoid floating point computation !!!) micro meters divided by micro seconds */
	wheelCircumference_microMeters = (uint32_t)WHEEL_CIRCUMFERENCE_MM * 1000;
	timeForRotation_microSecs = (uint32_t)timerTicksAvg * 64;								// 64 uS is the period between 2 timer ticks
	actualWheelSpeed = (wheelCircumference_microMeters / timeForRotation_microSecs) * 36;	// The result is km/h * 10 to avoid float
	return actualWheelSpeed;
}

uint16_t computeCrankCadence(volatile uint16_t crankTimerTicksBuf[TIMER_TICKS_BUFFER_SIZE]) {
	uint32_t timerTicksTotal = 0;
	uint16_t timerTicksAvg = 0;
	int16_t timeForRotation_milliSecs = 0;
	uint16_t actualCrankCadence = 0;

	/* Calculate average over the wheelTimerTicksBuf buffer */
	for (uint8_t i = 0; i < TIMER_TICKS_BUFFER_SIZE; i++) {
		timerTicksTotal += crankTimerTicksBuf[i];
	}
	timerTicksAvg = timerTicksTotal / TIMER_TICKS_BUFFER_SIZE;

	/* Calculate the actual cadence in RPM */
	timeForRotation_milliSecs = ((uint32_t)timerTicksAvg * 64) / 1000;		// Calculate the time for a rotation in micros and scale it up to millis by dividing with 1000
	if (timeForRotation_milliSecs < 1) return 0;							// This avoids zero division in the next operation
	actualCrankCadence = MINUTE_IN_MILLIS / timeForRotation_milliSecs;
	//if (actualCrankCadence > 200) actualCrankCadence = 200;					// This saturates the cadence to 200 if for whatever reason a higher cadence will occur
	return actualCrankCadence;
}

uint16_t computePedalPower(volatile uint16_t loadCellValuesBuf[LOAD_VALUES_BUFFER_SIZE], uint16_t crankCadence) {
	uint16_t loadValuesTotal = 0, loadValuesAvg = 0, prevLoadValuesAvg = 0, pedalForce = 0;
	uint16_t angularSpeed, angularAcceleration, momentOfInertia;
	uint16_t pedalPower = 0;
	static uint8_t constantLoad = 0;

	/* Calculate average over the loadCellValuesBuf buffer */
	for (uint8_t i = 0; i < LOAD_VALUES_BUFFER_SIZE; i++) {
		loadValuesTotal += loadCellValuesBuf[i];
	}
	loadValuesAvg = loadValuesTotal / LOAD_VALUES_BUFFER_SIZE;

	/* If 50 or more load values are the same, there is either a problem with the load cell or no pressure is on the pedal, therefore return max uint16_t value*/
	//if (loadValuesAvg == prevLoadValuesAvg) constantLoad += 1;
	//else { constantLoad = 0; prevLoadValuesAvg = loadValuesAvg; };
	//if (constantLoad >= 50) return 0xFFFF;

	pedalForce = (loadValuesAvg * 98) / 10; // load [kg*10] * factor [9.8*10] => pedalForce scaled with 100 and then scaled down with 10 [Newtons * 10]
	angularSpeed = ((SCALED_PI / 30) * crankCadence) / 100;	// compute angular velocity upscaled with 1000 and then scaled back down with 100 [rad/s * 10]
	
	//pedalPower = (pedalForce / SCALED_SAMPLING_TIME) * CRANK_LEG_LENGTH_CM * (SCALED_PI / 2) * 2;	// FIRST TRY
	pedalPower = ((uint32_t)pedalForce * (uint32_t)CRANK_LEG_LENGTH_CM * (uint32_t)angularSpeed) / 10000;	// 10000 - scaling factor
	
	//angularAcceleration = (angularSpeed - prevAngularSpeed) * 1000 / 88;	// scaled both the denominator with 1000 so we must scale the numerator with 1000
	//prevAngularSpeed = angularSpeed;
	//momentOfInertia = loadValuesAvg * (CRANK_LEG_LENGTH * CRANK_LEG_LENGTH);
	//pedalPower = momentOfInertia * angularAcceleration * angularSpeed;
	
	return pedalPower;
}
