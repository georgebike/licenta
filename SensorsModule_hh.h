//#pragma once
#ifndef SENSORMODULE_HH
#define SENSORMODULE_HH

#include "ucActuator_kh.h"

uint16_t computeWheelSpeed(volatile uint16_t[TIMER_TICKS_BUFFER_SIZE]);
uint16_t computeCrankCadence(volatile uint16_t[TIMER_TICKS_BUFFER_SIZE]);
uint16_t computePedalPower(volatile uint16_t[LOAD_VALUES_BUFFER_SIZE], uint16_t);

#endif // !SENSORMODULE_HH

