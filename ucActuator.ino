/*
 Name:		ucActuator.ino
 Description:	This module handles the management of hardware components.
				It runs 3 preemptive tasks, all handling specific operations 
				(reading sensor data, computing sensor readings, sending data to raspberry pi via serial comm)
 Created:	10/31/2018 2:32:34 PM
 Author:	George Catargiu
*/

#include <ir_Lego_PF_BitStreamEncoder.h>
#include <IRremoteInt.h>
#include <IRremote.h>
#include <boarddefs.h>
#include <Arduino_FreeRTOS.h>
#include "SensorsModule_hh.h"
#include "ucActuatorInit_hh.h"

IRrecv irrecv(IR_RECV_PIN);
decode_results results;

/* Task Prototypes */
void shortTask(void *pvParameters);		// shortTask will run periodically with a time period of 17.5 ms
void mediumTask(void *pvParameters);	// mediumTask will run periodically with a time period of 87.5 ms
void longTask(void *pvParameters);		// longTask will run periodically with a time period of 175 ms
										// 17.5 ms is the watchdog timer's time slice with a ~120kHz clock and 2048 prescaler

/* Functions Prototypes */
void writeSerial(uint16_t wheelspeed, uint16_t crankcadence, uint16_t pedalpower);	// Function for converting msg to bytes and sending it via serial

/* Global Variables*/
volatile uint8_t timer4_overflowCounter = 0, timer5_overflowCounter = 0;
volatile uint16_t wheelTimerTicksBuf[TIMER_TICKS_BUFFER_SIZE], crankTimerTicksBuf[TIMER_TICKS_BUFFER_SIZE];		// declare buffers for wheel and crank encoder Timer Ticks
volatile uint16_t loadCellValuesBuf[LOAD_VALUES_BUFFER_SIZE];	// declare buffer for load cell values. Will use uint16_t cuz the values are scaled with 100 for fixed point comp.
uint16_t wheelSpeed = 0, crankCadence = 0, pedalPower = 0;
uint8_t loadCellValuesBuf_index = 0;

/***** Main setup method - the entry point of the application *****/

void setup() {
	Serial.begin(115200);

	initPinout();

	/* Create tasks that will automatically be added to the Scheduler */
	xTaskCreate(shortTask,	(const portCHAR *) "Short and high priority task",		255, NULL, 2, NULL);	// 3 - highest priority; 0 - lowest priority
	xTaskCreate(mediumTask, (const portCHAR *) "Medium task with medium priority",	255, NULL, 1, NULL);
	xTaskCreate(longTask,	(const portCHAR *) "Long and low priority task",		255, NULL, 0, NULL);

	initTimers();

	irrecv.enableIRIn();	// start the IR receiver
	
	/* Start the actual scheduller that will call each task based on their period and priority */
	sei();		// General Interrupts Enable
	vTaskStartScheduler();
}

/***** Methods' definitions *****/
void shortTask(void *pvParameters) {
	/* This task reads data from sensors every time slice (17.5 ms) */

	(void*)pvParameters;
	TickType_t xLastWakeTime = xTaskGetTickCount();	

	/* Task will run undefinitely with the SHORT_PERIOD_TICKS period */
	for (;;) {
		digitalWrite(SHORT_TASK_MONITOR_PIN, !digitalRead(SHORT_TASK_MONITOR_PIN));

		uint16_t loadValue;
		if (irrecv.decode(&results)) {
			if (results.value != 0) loadValue = results.value; // convert from bytestring to uint16_t
			if (loadValue > 510) loadValue = 0;	// if we receive noisy out of bounds value drop that value
			if (loadCellValuesBuf_index >= LOAD_VALUES_BUFFER_SIZE) loadCellValuesBuf_index = 0;
			loadCellValuesBuf[loadCellValuesBuf_index] = loadValue;
			loadCellValuesBuf_index += 1;
			irrecv.resume();
		}
		//if (loadCellValuesBuf_index >= LOAD_VALUES_BUFFER_SIZE) loadCellValuesBuf_index = 0;
		//loadCellValuesBuf[loadCellValuesBuf_index] = 300;//loadValue;
		//loadCellValuesBuf_index += 1;

		vTaskDelayUntil(&xLastWakeTime, SHORT_PERIOD_TICKS);
	}
}

void mediumTask(void *pvParameters) {
	/* This task computes the data from sensors every 5 slices (87.5 ms) */

	(void*)pvParameters;
	TickType_t xLastWakeTime = xTaskGetTickCount();

	/* Task will run undefinitely with the MEDIUM_PERIOD_TICKS period */
	for (;;) {
		digitalWrite(MEDIUM_TASK_MONITOR_PIN, !digitalRead(MEDIUM_TASK_MONITOR_PIN)); // Visual representation of the task cyclic execution (oscilloscope)
		
		wheelSpeed = computeWheelSpeed(wheelTimerTicksBuf);
		crankCadence = computeCrankCadence(crankTimerTicksBuf);
		pedalPower = computePedalPower(loadCellValuesBuf, crankCadence);
		vTaskDelayUntil(&xLastWakeTime, MEDIUM_PERIOD_TICKS);	// Necessary to make the task period constant
	}
}

void longTask(void *pvParameters) {
	/* This task sends computed data to Raspberry Pi via serial every 10 slices (175 ms) */

	(void*)pvParameters;
	TickType_t xLastWakeTime = xTaskGetTickCount();

	/* Task will run undefinitely with the LONG_PERIOD_TICKS period */
	for (;;) {
		digitalWrite(LONG_TASK_MONITOR_PIN, !digitalRead(LONG_TASK_MONITOR_PIN));
		
		if (timer4_overflowCounter > 1) {	// if timer4 overflowed 2 or more times it means that no encoder interrupt occured so the wheel is not spinning
			wheelSpeed = 0;
		}
		if (timer5_overflowCounter > 1) {	// if timer5 overflowed 2 or more times it means that no encoder interrupt occured so the crank is not spinning
			crankCadence = 0;
		}
		writeSerial(wheelSpeed, crankCadence, pedalPower);
		vTaskDelayUntil(&xLastWakeTime, LONG_PERIOD_TICKS);
	}
}

void writeSerial(uint16_t wheelspeed, uint16_t crankcadence, uint16_t pedalpower) {
	UNION_SER_HELPER_UINT16 serHelper;
	serHelper.allDataValue.wheelSpeed = wheelspeed;
	serHelper.allDataValue.crankCadence = crankcadence;
	serHelper.allDataValue.pedalPower = pedalpower;
	Serial.write(serHelper.bytesValue, 6);	// Send bytes via serial in LITTLE ENDIAN order
}

/***** Interrupt Routines *****/

/* Timer 4 captures the timer ticks on wheel encoder interrupt (ICP4 - PIN 49) */
ISR(TIMER4_CAPT_vect) {
	static uint8_t wheelTimerTicksBuf_index = 0;
	static uint16_t currentTimerValue = 0, prevTimerValue = 0;
	timer4_overflowCounter = 0;
	uint8_t lowByte = ICR4L;
	uint8_t highByte = ICR4H;
	currentTimerValue = ((highByte & 0xFF) << 8) | (lowByte & 0xFF);	// Concat the log and high bytes to create the actual "word" unsigned integer
	
	/* Check if buffer reached its end and if it did, jump to the beggining */
	if (wheelTimerTicksBuf_index >= TIMER_TICKS_BUFFER_SIZE) {
		wheelTimerTicksBuf_index = 0;
	}

	/* Save the timer ticks count between 2 encoder interrupts in a circular volatile buffer */
	if (currentTimerValue > prevTimerValue) {
		wheelTimerTicksBuf[wheelTimerTicksBuf_index] = currentTimerValue - prevTimerValue;
		wheelTimerTicksBuf_index += 1;
	}
	else {		// Timer Overflow occured so the previous value will be higher than the current one
		wheelTimerTicksBuf[wheelTimerTicksBuf_index] = 65535 - prevTimerValue + currentTimerValue;
		wheelTimerTicksBuf_index += 1;
	}
	prevTimerValue = currentTimerValue;
	//Serial.println(wheelTimerTicksBuf[wheelTimerTicksBuf_index - 1]);
}

ISR(TIMER4_OVF_vect) {
	timer4_overflowCounter += 1;
}

/* Timer 5 captures the timer ticks on crank encoder interrupt (ICP5 - PIN 48) */
ISR(TIMER5_CAPT_vect) {
	static uint8_t crankTimerTicksBuf_index = 0;
	static uint16_t currentTimerValue = 0, prevTimerValue = 0;
	timer5_overflowCounter = 0;
	uint8_t lowByte = ICR5L;
	uint8_t highByte = ICR5H;
	currentTimerValue = ((highByte & 0xFF) << 8) | (lowByte & 0xFF); // Concat the log and high bytes to create the actual "word" unsigned integer

	/* Check if buffer reached its end and if it did, jump to the beggining */
	if (crankTimerTicksBuf_index >= TIMER_TICKS_BUFFER_SIZE) {
		crankTimerTicksBuf_index = 0;
	}

	/* Save the timer ticks count between 2 encoder interrupts in a circular volatile buffer */
	if (currentTimerValue > prevTimerValue) {
		crankTimerTicksBuf[crankTimerTicksBuf_index] = currentTimerValue - prevTimerValue;
		crankTimerTicksBuf_index += 1;
	}
	else {		// Timer Overflow occured so the previous value will be higher than the current one
		crankTimerTicksBuf[crankTimerTicksBuf_index] = 65535 - prevTimerValue + currentTimerValue;
		crankTimerTicksBuf_index += 1;
	}
	prevTimerValue = currentTimerValue;
}

ISR(TIMER5_OVF_vect) {
	timer5_overflowCounter += 1;
}

// THE MAIN LOOP WILL BE AVOIDED
void loop() {
	// NOTHING RUNS IN THE MAIN LOOP  
}
