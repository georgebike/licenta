#pragma once
#ifndef UCACTUATOR_INITHEADER
#define UCACTUATOR_INITHEADER
#include "ucActuator_kh.h"

/* PinModes initialization macro */
#define initPinout() \
	pinMode(SHORT_TASK_MONITOR_PIN, OUTPUT);	\
	pinMode(MEDIUM_TASK_MONITOR_PIN, OUTPUT);	\
	pinMode(LONG_TASK_MONITOR_PIN, OUTPUT);		\
	pinMode(INTERRUPT_TEST_PIN, OUTPUT);		\
	pinMode(WHEEL_ENCODER_PIN, INPUT);			\
	digitalWrite(WHEEL_ENCODER_PIN, HIGH);		\
	pinMode(CRANK_ENCODER_PIN, INPUT);			\
	digitalWrite(CRANK_ENCODER_PIN, HIGH);		\

/* Timer4 and Timer5 initialization macro (Timer4 used for wheelSpeed and Timer5 used for crankCadence) */
#define initTimers()	\
	/* Timer 1 configuration for crank encoder (ICP5 - pin 48) */	\
	TCCR5A = 0x00;	\
	TCCR5B = (1 << CS52) | (1 << CS50);					/* prescaler at 1024 */ \
	TIMSK5 = (1 << ICIE5) | (1 << TOIE5);				/* set timer input capture interrupt enable, timer overflow interrupt enable */		\
	TIFR5 = (1 << ICF5) | (1 << TOV5);					/* clear interrupt flags for input capture and overflow */		\
	/* Timer 2 configuration for wheel encoder (ICP4 - pin 49) */			\
	TCCR4A = 0x00;	\
	TCCR4B = (1 << CS42) | (1 << CS40);					/* prescaler at 1024 */		\
	TIMSK4 = (1 << ICIE4) | (1 << TOIE4);				/* set timer input capture interrupt enable, timer overflow interrupt enable */		\
	TIFR4 = (1 << ICF4) | (1 << TOV4);					/* clear interrupt flags for input capture and overflow */		\

#endif // !UCACTUATOR_INITHEADER

