//#pragma once
#ifndef UCACTUATOR_CONFIGHEADER
#define UCACTUATOR_CONFIGHEADER

#include "Arduino.h"

/* Arduino Mega 2560 interrupt pins: 2, 3, 18, 19, 20, 21 */

////////// VALUES //////////
#define SHORT_PERIOD_TICKS		1
#define MEDIUM_PERIOD_TICKS		5
#define LONG_PERIOD_TICKS		10
#define TIMER_TICKS_BUFFER_SIZE 10
#define LOAD_VALUES_BUFFER_SIZE 3
#define MINUTE_IN_MILLIS		60000
#define WHEEL_CIRCUMFERENCE_MM  2115	// Bike wheel circumference in millimeters
#define CRANK_LEG_LENGTH_CM		18		// The size of the crank leg in centimeters
#define SCALED_PI				3141	// Value of PI scaled with 1000
#define SCALED_SAMPLING_TIME	87		// Medium task period scaled with 1000	0.0875 -> 87

/* Struct that incapsulates the values that are sent to raspberry via Serial USB */
typedef struct allDataStruct {
	uint16_t wheelSpeed;
	uint16_t crankCadence;
	uint16_t pedalPower;
}allData;
typedef union uint16_ser_helper {		// helper union to convert unsigned int into bytes array to be streamed via serial comm
	allData		allDataValue;
	uint8_t		bytesValue[6];
}UNION_SER_HELPER_UINT16;


////////// PINS //////////
#define SHORT_TASK_MONITOR_PIN	13
#define MEDIUM_TASK_MONITOR_PIN 12
#define LONG_TASK_MONITOR_PIN	11
#define IR_RECV_PIN				3	// this pin is the signal receiving from IR receiver
#define INTERRUPT_TEST_PIN		8	// this pin is used only for testing & debug purposes (should be deleted before deployment)
#define WHEEL_ENCODER_PIN		49	// this pin is used as ICP4 (interrupt capture on timer 4)
#define CRANK_ENCODER_PIN		48	// this pin is used as ICP5 (interrupt capture on timer 5)

#endif


